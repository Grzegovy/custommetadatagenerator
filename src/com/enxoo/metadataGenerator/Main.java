package com.enxoo.metadataGenerator;

import java.io.File;

public class Main {

    /*
        Arg 1: [Required] File to split
        Arg 2: [Required] Template File
        Arg 3: [Optional] CSV Column mapping config
     */
    public static void main(String[] args) {
        Integer argumentsSize = args.length;
        if(argumentsSize < 2) {
            System.out.println("Error! Expected 2 or more input arguments!");
            System.out.println("Arg 1: [Required] File to split");
            System.out.println("Arg 2: [Required] Template File");
            System.out.println("Arg 3: [Optional] CSV Column mapping config");
            return;
        }

        System.out.println("Working Directory = " + System.getProperty("user.dir"));

        try {
            Util.loadTemplateFile(args[1]);
            Util.loadCsvColumnsTranslationMap((argumentsSize >= 3) ? args[2] : "fileTokenToCsvColumnMap.json");
            System.out.println("Translations loaded: " + Util.csvColumnsTranslationMap);
            Util.loadCSV(args[0]);
            System.out.println("Records loaded: " + Util.records.size());
            Util.deleteDir(new File(Util.RESULT_DIR));
            Util.processCSV();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
