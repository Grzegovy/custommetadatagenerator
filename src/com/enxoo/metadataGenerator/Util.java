package com.enxoo.metadataGenerator;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class Util {
    public static final String COMMA_DELIMITER = ";";
    public static final String FILE_NAME_TOKEN = "[[File_Name]]";
    public static final String METADATA_LABEL_TOKEN = "[[File_Name]]";
    public static final String FILE_METADATA_EXTENSION = ".md-meta.xml";
    public static final String RESULT_DIR = "result/";

    public static HashMap<String, Integer> csvColumnsTranslationMap;
    public static List<List<String>> records = new ArrayList<>();
    public static String METADATA_TEMPLATE = null;
    public static List<String> errors = new ArrayList<>();

    public static void loadCsvColumnsTranslationMap(String filePath) throws Exception {
        String configString = new String(Files.readAllBytes(Paths.get(filePath)));
        ObjectMapper objectMapper = new ObjectMapper();
        csvColumnsTranslationMap = objectMapper.readValue(configString, HashMap.class);

        if(!csvColumnsTranslationMap.containsKey(FILE_NAME_TOKEN)) {
            throw new Exception(FILE_NAME_TOKEN + " token is missing in token to CSV column mapping file!");
        }

        if(!csvColumnsTranslationMap.containsKey(METADATA_LABEL_TOKEN)) {
            throw new Exception(METADATA_LABEL_TOKEN + " token is missing in token to CSV column mapping file!");
        }
    }

    public static void loadCSV(String filePath) throws Exception {
        try(BufferedReader br = new BufferedReader(new FileReader(filePath, StandardCharsets.UTF_8))) {
            for(String line; (line = br.readLine()) != null; ) {
                readCsvLine(line);
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public static void readCsvLine(String line) {
        System.out.println("Processing... " + line);
        if(line != null && line != "") {
            String[] values = line.split(COMMA_DELIMITER);
            records.add(Arrays.asList(values));
        }
    }

    public static void loadTemplateFile(String path) throws FileNotFoundException {
        METADATA_TEMPLATE = new Scanner(new File(path)).useDelimiter("\\Z").next();
        System.out.println("Loaded Template matadata file: \n" + METADATA_TEMPLATE);
    }

    public static void processCSV() {
        // For each line CSV
        for(List<String>  line : records.subList(1,records.size()))  {
            String fileBody = METADATA_TEMPLATE;
            String fileName = RESULT_DIR + line.get(csvColumnsTranslationMap.get(FILE_NAME_TOKEN)) + FILE_METADATA_EXTENSION;

            // Replace all tokens in file
            for (Map.Entry<String, Integer> entry : csvColumnsTranslationMap.entrySet()) {
                String token = entry.getKey();
                Integer columnIndex = entry.getValue();
                String valueToReplace = (line.size() > columnIndex) ? line.get(columnIndex) : "";
                fileBody = fileBody.replace(token, valueToReplace);
            }
            // Save output file:
            saveMetadataFile(fileName, fileBody);
        }

        // Show errors:
        System.out.println("Error messages: " + errors.size());
        errors.forEach(string -> System.out.println(string));
    }

    public static void saveMetadataFile(String fileName, String fileBody) {
        try {
            createFile(fileName);
            saveContentToFile(fileName, fileBody);
        } catch (Exception e) {
            errors.add(fileName + "Error: " +  e.getMessage());
        }
    }

    public static void deleteDir(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (int i=0; i<children.length; i++) {
                deleteDir(new File(dir, children[i]));
            }
        }
        // Remove old dir
        dir.delete();
    }

    public static void createFile(String name) throws Exception {
        File myObj = new File(name);
        myObj.getParentFile().mkdirs();

        if (myObj.createNewFile()) {
            System.out.println("File created: " + myObj.getName());
        } else {
            System.out.println("File already exists.");
        }
    }

    public static void saveContentToFile(String fileName, String fileContent) throws Exception {
        Writer myWriter = new OutputStreamWriter(new FileOutputStream(fileName), StandardCharsets.UTF_8);
        myWriter.write(fileContent);
        myWriter.close();
    }
}
